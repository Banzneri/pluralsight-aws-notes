# Contents

## [The Big Picture](#aws-developer-the-big-picture)
1. [What is AWS?](#1-what-is-aws)
2. [Understanding the Core Services of AWS](#2-understanding-the-core-services-of-aws)
3. [Enhancing Your App with AWS Databases and Application Services](#3-enhancing-your-app-with-aws-databases-and-application-services)
4. [Harnessing the Power of AWS from the Command Line to Code](#4-harnessing-the-power-of-aws-from-the-command-line-to-code)

## [Getting Started](#aws-developer-getting-started)
1. [Welcome to AWS](#1-welcome-to-aws)
2. [Sounding the Alarm with IAM and CloudWatch](#2-sounding-the-alarm-with-iam-and-cloudwatch)
3. [Getting Inside the Virtual Machine with EC2 and VPC](#3-getting-inside-the-virtual-machine-with-ec2-and-vpc)
4. [Hosting All the Things with S3](#4-hosting-all-the-things-with-s3)
5. [A Tale of two databases with DynamoDB and RDS](#5-a-tale-of-two-databases-with-dynamodb-and-rds)
6. [Automating Your App with Elastic Beanstalk and CloudFormation](#6-automating-your-app-with-elastic-beanstalk-and-cloudformation)
7. [Speeding Up with CloudFront and ElastiCache](#7-speeding-up-with-cloudfront-and-elasticache)

## [Designing and Developing](#aws-developer-designing-and-developing)
1. [Designing and Developing on AWS](#1-designing-and-developing-on-aws)
2. [Launching Instances in AWS](#2-launching-instances-in-aws)
3. [Scalable Computing in AWS](#3-scalable-computing-in-aws)'
4. [Storage in AWS](#4-storage-in-aws)


## Important services

- AWS LAMBDA
- DynamoDB
- AWS Cognito
- Elastic Beanstalk
- Step functions
- Amazon Cognito and IAM
- AWS CLI, SDK and CloudFormation
- ECS
- X-Ray
- CodeCommit, CodeBuild, CodeDeploy, CodePipeline

# AWS Developer: The Big Picture

## 1. What is AWS?

### Core services:

- Elastic Cloud Compute (EC2)
- Simple Storage Service (S3)
- Relational Database Service (RDS)
- Route 53

### Additional (AWS Databases and Application Services)

- Elastic Beanstalk (EB)
- Lambda
- DynamoDB
- Virtual Private Cloud (VPC)
- CloudFront
- CloudWatch

### Summary

- Reasons to use the cloud
- The birth of AWS
- Price, scalability, and location
- AWS service interactions
- Azure, GCP, Heroku etc...

## 2. Understanding the Core Services of AWS

### EC2

- Basic unit of computing, think of it as a virtual machine where you can run an application
- Possible use cases:
  - Run applications
  - Virtual desktop
  - 3rd party software
  - Computing in general
- Instances running computing operations can increase or decrease at will, hence the name Elastic (scalability)
- When launching an EC2 instance, you first have to choose an Amazon Machine Image (AMI), which is an **operating system and software used on an EC2 instance**
- After choosing an AMI, you have to choose an instance type, i.e. the specs for your instance (number of CPUs, amount of RAM, network performance)
- After choosing an instance type, you must configure your instance details (**number of instances**, which is used when Auto Scaling Group is created, which are rules for automatically scaling EC2 instances up and down, **security roles**. etc)
- Next step is to add storage, EBS
  - Use EBS for EC2 file systems
  - Use S3 for file storage
- Next you have to configure security groups
  - IP-based communication rules for a single or group of EC2 instances
- Example Security Group scenarios
  - Control who can SSH into EC2 instance
  - Allow access between EC2 instances
  - Allow access to databases
  - Accept HTTP requests
- Final step is to create the instance with an existing key pair, which allows you to SSH into the instance and make whatever modifications you want
- EC2 instances are charged by the hour
- Prices change based on:
  - Instance type
  - AMI type

### S3

- Static file hosting service
- Can be used in conjunction with EC2
- Although you can host files to end users, that's not all it's used for
- Sort of a catch-all service used by pther services in AWS, such as CloudWatch & Elastic Beanstalk
- Maximum size of 5 TB
- Buckets are the foundational structure in S3
- A bucket is your root resource to which you can add, delete, or modify objects
- S3 buckets can
  - Trigger events when objects are added/modified/deleted
  - Preserve old versions of any objects
  - Replicate objects across regions
- S3 are assigned URLs to which you can access the objects contained within them
- Can be configured for static website hosting
- To solve latency in S3, use CloudFront
- S3 pricing structure
  - Amount of data stored
  - Number of requests
  - Amount of data transferred
  - Prices vary by region

### RDS

- An umbrella term for several different managed relational databases
- Managed database aspects
  - Scheduled automated backups
  - Simple software updates
  - Managed infrastructure
- Database options
  - MySQL
  - PostgreSQL
  - Microsoft SQL Server
  - MariaDB
  - Oracle Database
  - Amazon Aurora
- RDS databases run on EC2 instances
- RDS makes it easy to:
  - Take DB snapshots
  - Change the hardware
- Security
  - Access is controlled via Security Groups
- RDS pricing structure
  - Type of database
  - Region
  - EC2 instance type

### Route 53

- DNS service that empowers EC2 instances and S3 buckets to be accessible via URLs
- Domain Name System is a system that translates human-readable URLs to IP addresses
- First you need to do is set up a hosted zone
  - Basically a root domain name, like example.com
- Health check service allows to set up regular checks for a given URL path

### Summary

- EC2 for computing
- S3 for storage
- RDS for databases
- Route53 for URLs

## 3. Enhancing Your App with AWS Databases and Application Services

### Elastic Beanstalk

- An application service that makes it easy to run your code and scale it on AWS
- Under the hood, it's simply running your code on EC2 instances
- Deploying you app to EC2 (without EB)
  - Manual configuration
  - Manual code deployment
  - Restricted command line interface
  - Scale with AMIs
  - Manual monitoring
- Deploying your app with EB
  - Easy deployment with various tools
  - Set it and forget it configuration
  - Aggregated monitoring and logging
- EB structure
  - Top level organization is the Application, which needs a unique name
  - Inside each Application can be many application versions
  - Application versions can be deployed into an environment (Test environment, Production environment)
  - An environment is the rules and configuration that manage actual EC2 instances
  - Application versions are stored in S3
  - Maximum number of application versions is defaulted to 1000
- Monitoring
  - Number of requests
  - CPU utilization
  - Network traffic
- EB pricing
  - Free
  - Only pay for EC2 instances, load balancers, and S3

### Lambda

- Provides function code execution as a service
- Sometimes called _Function as a Service (FAAS)_ or _serverless_
- Features
  - Executes code
  - No server management required
  - Only pay when you code is running
- Structure
  - Top level Function
  - Inside Function
    - Code
    - Platform type (Node, Python, Go, etc)
    - Triggers (invocation targets)
      - Api Gateway
      - CloudWatch
      - CloudFront
      - etc
    - Configuration
      - Execution timeout
      - Memory requirements
      - IAM role Lambda should execute with
- Great for small, irregular tasks
  - Nightly ETL kickoffs
  - Notification-type functions

### DynamoDB

- A managed NoSQL database service
- Supports both document and key value store models
- Features
  - Unlimited, elastic storage
  - No hardware choices
  - Pay only for what you use
- Core structure is a table, which is your root point of data storage
- Each table need a primary key for partitioning and indexing the data, optionally secondary indexes as well
- Provisioned Throughput Capacity
  - Number of read/write units per second
- Pricing
  - Provisioned throughput capacity
  - Amount of data stored
  - Varies by region

### Virtual Private Cloud (VPC)

- Allows to secure you resources into groups that follow access rules and share a logical space
- Commonly used when launching EC2 instances in order to easily secure and control access to them
- Security Groups secure single instances
- VPCs secure groups of instances
- Features
  - Configure VPC routing tables
  - Use NAT gateways for outbound traffic
  - Internal IP address allocation
- Structure
  - Inside each AWS Virtual Private Cloud are one or more subnets
  - Subnets are a further way to group you resources and assign different rules to each
  - One of the main reasons for using multiple subnets within a VPC is to set up both private and public subnets
  - Private subnet
    - Houses your databases and application instances
    - No access to the internet at all
  - Public subnet
    - Access to the internet
    - Could utilize Security Groups to make it secure
  - The private subnet may use a NAT gateway in the public subnet to access the internet securely
  - VPC is another resource to increase the security of AWS instances
  - Security
    - Routing table
    - Network access control lists (ACL), which act as subnet-level firewalls
- Is free

### CloudWatch

- Monitoring service for many different other services in AWS
- Has been integrated into most of the other AWS services for monitoring & alarms
- Functionality
  - Monitoring resources
  - Acting on alerts
- Alarms
  - For each alarm, you choose from a pre-existing set of metrics for each service
  - Examples
    - EC2 - CPUUtilization
    - DynamoDB - ConsumedReadCapacityUnits
    - S3 - NumberOfObjects
    - Route53 - HealthCheckStatus
    - RedShift - DatabaseConnections
  - Actions
    - SMS Notification
    - Autoscale EC2 instances
- Can also consume, aggregate, and monitor logs
  - Accomplished by installing & configuring an awslogs agent on EC2 instances and telling which logs to send to CloudWatch
  - Can be configured to filter the logs & send alerts based on criteria you define
    - For example tracking how many times a certain exception happens in your logs & sending a notification once it's occurred a set number of times

### CloudFront

- A content delivery network that allows you to serve files globally with very fast connections
- Works seemlessly with S3, EC2, AWS load balancers, and Route53 to serve your content from the location closest to incoming requests
- Structure
  - Start by creating a CloudFront distribution
    - Defines a set of content to be served from CloudFront
    - For each distribution, define an original location for the content, such as an S3 bucket
    - Once created, a unique CloudFront URL will be assigned to the distribution
- Configuration
  - SSL certificates
  - Allowed HTTP methods
  - Edge locations

### Summary

- EB for applications
- Lambda for functions
- DynamoDB for NoSQL
- VPC for networking
- CloudWatch for monitoring
- CloudFront for edging

## 4. Harnessing the Power of AWS from the Command Line to Code

### Web Console

- All services section lists all the services available in AWS
- As a developer you're likely to use only less than 10 services
- Don't forget regions!
  - It's important you consider which region you're launching your resources into because many resources are not visible to others across regions
  - Select region at the top right corner
- Billing section lists your balance, and month-to-date spend by service, so you can see how much you are paying per service
- Remember to setup Multi-factor authentication (MFA)

### Software Development Kits

- Supported
  - Android
  - iOS
  - JavaScript & NodeJS
  - Java
  - Ruby and Rails
  - PHP
  - Go
  - .NET
  - C++
  - Unity
  - And others
- Check any AWS SDK for updates, sometimes they deprecate them
- AWS SDK operation examples
  - Read/write to RDS
  - Modify CloudWatch rules
  - Invalidate CloudFront distribution
  - Essentially anything you can do with the web console, you can do with an AWS SDK
- Code example

  ```js
  const AWS = require("aws-sdk");

  AWS.config.update({ region: "us-east-1" });

  const dynamodb = new AWS.DynamoDB();

  const item = {
    Item: {
      id: {
        S: new Date().getTime().toString(),
      },
      value: {
        S: process.argv[2],
      },
    },
    TableName: "aws-developer",
  };

  dynamodb.putItem(item, (err, res) => {
    if (err) throw err;
  });
  ```

### Command Line Interface

- A unified tool to interact with different AWS resources from a command line
- Ways to use the AWS CLI
  - Typing manually in a command line
  - Inside shell scripts or batch files, which is useful for CI/CD pipelines
- Command structure
  ```
  aws <service> <command> <arguments>
  ```
- Previous code example using the CLI
  ```
  aws dynamodb put-item --table-name aws-developer --item <item in json format>
  ```

### Summary

- Web console in the browser
- SDKs in your code
- CLI in your command line

# AWS Developer: Getting Started

## Overview

- Learning the fundamentals of developing & deploying web applications to AWS
- Deploying scalable applications with EC2 & Elastic Beanstalk
- Storing static content in S3 & edging it with CloudFront
- Creating reproducible infrastructure with CloudFormation templates
- Using managed databases with RDS & DynamoDB

## 1. Welcome to AWS

### Developing in the Cloud

- Similarities with local development
  - Application runs on OS/Platform
  - Requires platform to be installed (eg. .NET)
  - HTTP servers listen on ports
  - Connects to outside databases
- Differencies with local development
  | Local                                  | Cloud                            |
  | -------------------------------------- | -------------------------------- |
  | Everything runs on same machine        | One service = One responsibility |
  | Files stored on server                 | Files served from S3             |
  | Database on same server as application | Database as a Service            |
  | Connections done manually              | Connections done with AWS SDK    |

###  The Web of Amazon Web Services
- Most services in AWS are accessed with a TCP connection
- Each instance of a service (resource) is given a local IP address with which other services can communicate with it
- You can also assign an external IP if needed

### AWS Tooling
- Tooling usage in course: 
  - 55% Web console
  - 40% SDK
  - 5% CLI
- AWS access key is needed to access using SDK & CLI

## 2. Sounding the Alarm with IAM and CloudWatch

### Simple Notification Service (SNS)

- Structure
  - Main structure is the Topic, which is used to create a unique Amazon resource name that can then have notifications sent to it
  - Once subscribed to a Topic with an email or SMS number, any notification sent to that SNS topic will then be communicated to your subcribed endpoint

### Identity and Access Management Overview (IAM)

- IAM user management aspects
  - Passwords
  - Multi-factor authenticaton (MFA)
  - Access keys
  - SSH keys
- Structure
  - Main structure is a Policy, which is a collection of permissions to access different services in a particular way
- IAM policy examples
  | Developer Group Policy | Tester Group Policy        |
  | ---------------------- | -------------------------- |
  | EC2 Full Access        | DynamoDB Read-Only         |
  |                        | S3 Full Access Test Bucket |
 
 ### Securing Your AWS Account

 - Root account permissions
   - Administer all services
   - Modify billing information
   - Manage users
 - MFA with AWS
   - "Know" a password
   - "Have" a device

### Understanding Policies

- Users have no permissions by default
- Policies can be applied to individual users or to groups
- Policy statement properties
  - Effect (Allow or Deny)
  - Action (Operation user can perform on services)
  - Resource (Specific resources user can perform action on)
- IAM policy types
  - AWS managed policy (General purpose, service-wide permissions)
  - Customer managed policy (Custom, specific resource permissions)

### Configuring Users and Groups

- Amazon suggests applying policies to groups instead of users since it's easier to manage permissions that way
- Users can be added to multiple groups, so groups can be as specific as you want

### Summary

- CloudWatch & SNS (Best friends forever)
- Setting up first alarm
- IAM security lockdown (users, policies, groups)
- Enabled MFA on a root account
- Using IAM user account instead of root is an Amazon-suggested best practice to avoid mistakes

## 3. Getting Inside the Virtual Machine with EC2 and VPC

### Overview

- VPC security blanket
- Creating subnets
- EC2 virtual empire
- Deploying the demo application to EC2
- Configure a production-ready setup for the app, complete with a load balancer & auto scaling group

### Virtual Private Cloud Overview

- With VPC, resources have privatrre IP addresses with which they communicate with each other
- Security Groups
  - Defines allowed incoming/outgoing IP addresses & ports. Kind of like a mini-firewall
  - You can set security group rules to allow access from other security groups instead of by IP
- Routing Table
  - Used to configure routing destinations for traffic coming out of the VPC
  - Each VPC has one routing table, which declares attempted destination IPs and where they should be routed to
  - For instance, if you want to run all outgoing traffic through a proxy, you can set a routing table rule to automatically send traffic to that IP
- Network Access Control List
  - Each VPC has one access control list, which acts as an IP filtering table, saying which IPs are allowed through the VPC for both incoming & outgoing traffic
  - They are kind of like superpowered security groups that apply rules for the entire VPC
- VPC defines a private, logically isolated area, for instances
- Instances can't be launched into just a VPC, they must also be launched into a subnet that is inside a VPC
- Subnets
  - An additional isolated area that has it's own [CIDR block](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing), routing table, and access control list
  - Enable to create different behavior in the same VPC, for instance creating a public subnet that can be accessed and have access to the public internet and a private subnet that is not accessible from the internet and must go through a NAT gateway to access the outside world

### Creating a Virtual Private Cloud

- When first going to the VPC dashboard, there is a default VPC that is pre-existing when you create AWS account
- Subnet can only exist in a single availability zone, so more subnets need to be created to deploy into different availability zones
- NAT gateways are really expensive, so in the course the subnets need to have public IP addresses (edit subnet settings > enable auto-assign public IPv4 address)

### Connecting to an EC2 instance

- Connecting to EC2 instance with SSH
  ```sh
  ssh -i <path-to-pem> ec2-user@<ec2-ip>
  ```
- [Elastic IP address](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html)
  - *"An Elastic IP address is a static IPv4 address designed for dynamic cloud computing. An Elastic IP address is allocated to your AWS account, and is yours until you release it. By using an Elastic IP address, you can mask the failure of an instance or software by rapidly remapping the address to another instance in your account. Alternatively, you can specify the Elastic IP address in a DNS record for your domain, so that your domain points to your instance. For more information, see the documentation for your domain registrar, or Set up dynamic DNS on Your Amazon Linux instance."*

### Updating and Deploying to an EC2 instance

- Node needs to be installed on the EC2 instance
  - To do that, we'll need to update our repositories in the YUM package manager and install it that way
    ```sh
    curl -sL https://rpm.nodesource.com/setup_16.x | sudo bash -
    ``` 
  - Then install node
    ```sh
    sudo yum install -y nodejs
    ```
- Transferring project files to the EC2 instance is done using [SCP](https://en.wikipedia.org/wiki/Secure_copy_protocol)
  ```sh
  scp -r -i <pem_file> <local_code> ec2-user@<ec2_ip>:home/ec2-user
  ```
  - Remember to delete the node_modules folder before transferring the files
  - Run ```npm install``` when connected to the EC2 instance to install dependencies

### Scaling EC2 instances

- Custom AMI
  - EC2 instance can be saved as a snapshot and replicated
  - It can then be copied to different instances
- Auto Scaling Group
  - Auto scaling group uses a launch template and scaling rules to expand or shrink a pool of instances automatically
- Load Balancer
  - Routing appliance that maintains a consistent DNS entry and balances requests to multiple instances
  - In other words, it provides a stable endpoint to reliably send your users and set DNS entries to
  - It will keep track of which IPs are available and send users to them efficiently
  - Connects to an auto scaling group, so they both work together to efficiently create groups of instances and route users to them

### Creating a Load Balancer

- Types of load balancers
  - [Application load balancer](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/introduction.html)
    - For web applications that use HTTP/HTTPS
  - [Network load balancer](https://docs.aws.amazon.com/elasticloadbalancing/latest/network/introduction.html)
    - When you need ultra-high performance, TLS offloading at scale, centralized certificate deployment, support for UDP, and static IP addresses for your application
  - [Gateway load balancer](https://docs.aws.amazon.com/elasticloadbalancing/latest/gateway/introduction.html)
    - When you need to deploy & manage a fleet of third-party virtual appliances that support GENEVE
  - Classic load balancer
    - Not relevant anymore
- Typical load balancer listeners
  - HTTP on port 80
  - HTTPS on port 443
- When a user logs in to an EC2 instance, we need to make sure that the user is logged in in other instances as well, to do this we need to enable stickiness on the load balancer
- Session stickyness is configured in the target group

### Scaling in Action

- Ways to generate requests
  - Open in browser without browser cache
  - Use JMeter
  - Use Apache Benchmark
- Using Apache Benchmark
  ```sh
  ab -n 100 -c 5 http://<url_to_load_balancer>/
  ```
  - This will send a total of 100 requests to the load balancer at a maximum of 5 requests at a time

## 4. Hosting All the Things with S3

### S3 Overview

- S3 stores Objects in the Region you specify
- S3 Object
  - Is basically a file & metadata
  - Metadata includes things like file type & modified date
  - Maximum object size is 5TB
  - Upload limit for a single put operation is 5GB
- S3 Bucket
  - Stores Objects
  - Create a URL namespace for those Objects
  - Example:
    - Region: Oregon (us-west-2)
    - Bucket name: pizza-luvrs
    - URL: s3-us-west-2.amazonaws.com/pizza-luvrs
  - URL must be unique, so when naming the bucket, the bucket name must be unique
- S3 Object key example
  - File name: image.png
  - Folder name: images
  - Object key: images/image.png
- You can set permissions to define who can view or edit S3
- Cross-region replication
  - A configured feature of S3 that will automatically copy any files added or modified in an S3 bucket to a different region
  - Will copy to only one other region
  - To have a worldwide reduction of latency, the best option is CloudFront

### Uploading Objects to S3

- Objects can be uploaded using either the console, AWS CLI, or the SDK
- Uploading using the CLI
  ```
  aws s3 cp <local_folder> s3://<bucket>/<remote_folder> --recursive --exclude "<pattern>"
  ```

## 5. A Tale of Two Databases with DynamoDB and RDS

### Relational Database Service (RDS) Overview

- RDS managed task example
  - Software upgrades
  - Nightly database backups
  - Monitoring
- Creates an EC2 instance under the covers, installing an appropriate operating system & database engine
- Choosing the correct EC2 instance type is very important, since an underpowered EC2 instance won't be able to process queries very well
- Instance type can be changed after the instance has been created
- Backups
  - Occurs daily
  - Configurable backup window
  - Backup can be stored from 1 day up to 35
- Multi-AZ deployment (Availability Zone)
  - Database replication to different AZ in the same region
  - Automatic failover in case of catastrophic event -> no downtime
- Database read replica
  - Non-production copy of database
  - Eventual consistency with source
  - Useful for running queries on data
  - Will not be used as failover

### Choosing a Database Engine in RDS

- RDS database options
  - MySQL
  - PostgreSQL
  - Microsoft SQL Server
  - MariaDB
  - Oracle Database
  - Amazon Aurora
- What is used locally? How much are you willing to spend? What do I have experience in? Which has the best client?

### Connecting to a PostgreSQL database with pgAdmin

- Make sure your RDS instance security group gives access to *your IP* on port 5432
  
### DynamoDB Overview

- DynamoDB on-demand capacity
  - Table capacity scales as needed
  - Pay per table or index request
  - More expensive than Provisioned Capacity mode
- DynamoDB provisioned capacity auto scaling
  - Increases and decreases provisioned capacity based on rules
  - Works like EC2 auto scaling groups
  - Cheaper than on-demand capacity mode

### Deciding between RDS and DynamoDB

- DynamoDB has storage flexibility
- RDS has query flexibility

### Connecting to DynamoDB with Code

- To run on EC2 add **AmazonRDSFullAccess** and **AmazonDynamoDBFullAccess** to the pizza-ec2-role **and** allow access to RDS instance from pizza-ec2-sg by adding to the RDS instance security group

### Summary

- Relational databse fundamentals
- Wrote code to connect to the database
- Keys to the table
- Dynamo launched

## 6. Automating Your App with Elastic Beanstalk and CloudFormation

### Overview

- CloudFormation fundamentals
- Use CloudFormation to create a stack
- Elastic Beanstalk introduction & automating deployment
- Create an environment and deploy the application

### CloudFormation Overview

- Service to provision resources using templates
- Resources already created
  - RDS database
  - DynamoDB table
  - VPC
  - Subnets
  - EC2 instance
  - IAM role
  - Security groups
  - Launch configuration
- Useful for example when creating a development stack to mirror the production stack
- CloudFormation template
  - JSON document
  - Contains configuration for resources
  - Can be used in version control
  - No limit to amount of resources
- CloudFormation stack
  - Essentially a group of AWS resources created by a single CloudFormation template
  - Template can be used to create more than one stack, but each stack needs to be named uniquely
- Other CloudFormation operations
  - Updating a Stack
  - Deleting a Stack
  - CloudFormation will attempt to process the update without even restarting your resources if it's possible

### Elastic Beanstalk Overview

- While CloudFormation automates the creation of infrastructure, Elastic Beanstalk automates the deployment of your application
- Handles scaling, monitoring, and any other resource provisioning required to get your app run efficiently in AWS
- All you need to do is upload your application code to Elastic Beanstalk, and it will take care of having the correct OS and platform installed for you application
  | CloudFormation            | Elastic Beanstalk                              |
  | ------------------------- | ---------------------------------------------- |
  | Only provisions resources | Provisions resources and runs your application |
- Elastic Beanstalk Application
  - Represents a logical application
  - Runs a single platform (eg. NodeJS, Java)
  - Has one or more application versions (stored in S3)
- Elastic Beanstalk environment
  - Resides inside Application
  - AMI, EC2 instances, auto scaling group
  - Application version
  - Useful to separate a production environment & development environment


### Deploying an Application with Elastic Beanstalk

- Application deployment steps
  | Manual                          | EB              |
  | ------------------------------- | --------------- |
  | Upload new code to EC2 instance | Upload new code |
  | Create AMI                      |                 |
  | Update launch template          |                 |
  | Update Auto Scaling Group       |                 |
  | Terminate out-of-date instances |                 |
- Configure options
  - Set VPC (Network tab)
  - Set security group (Instances tab)
  - Set key pair (Security tab)
  - Set role (Security Tab)
  - Set load balancer & auto scaling (Capacity tab)
  - Configure load balancer (Load Balancer tab)
    - Set Process to listen on the port your application uses (3000 in this case)
- Elastic Beanstalk uses CloudFormation to manage a lot of the application creation

### Configuring an Elastic Beanstalk environment

- Navigate to IAM dashboard
  - Attach policies to access databases and AWSElasticBeanstalkWebTier
- Ensure that the existing RDS instance is configured to allow access from our EC2 instances
- Changes to an Elastic Beanstalk environemnt config may require the instances to restart

### Summary

- Using CloudFormation to create a template to provision resources
- Using CloudFormation template to create a replica of our pizza-luvrs infrastructure
- Using Elastic Beanstalk and using environments to easily deploy an application
- Zipped pizza-luvrs project and deployed it using a new Elastic Beanstalk environment what we configured

## 7. Speeding Up with CloudFront and ElastiCache

### CloudFront Overview

- Only way to reduce latency is to reduce distance between user and app
- CloudFront is a global content delivery network designed to reduce latency and reduce application load
- Requests to your application will automatically route to the CloudFront edge location nearest to the requester
- Integrates with S3, EC2, and load balancers
- Edges "Objects" and servers them directly
- Proxies dynamic content to origin source
- A set of content edged with CloudFront is called a distribution
- Distribution is given its own URL on the cloudfront.net domain
- Each distribution provides content from one or more origins, like S3 buckets, EC2 instances, and load balancers
- CloudFront distribution behaviors
  - Determines cache behavior based on path
  - Set time-to-live for specific content
  - Fine-tuned control over caching behavior

### ElastiCache Overview

- ElastiCache is a managed service for in-memory cache datastore
- Databases are important -> there are times when applications need the kind of big data of databases, but need much faster retrieval
- For this purpose, in-memory cache data stores were created
- Most popular solutions are Redis and Memcached, both of which are offered by ElastiCache
- Extremely similar to RDS in instrumentation
- Features:
  - Managed maintenance, upgrades, etc.
  - Automatic read replicas
  - Simple node management
- Structure
  - The main structure is the cluster
  - Cluster is a collection of nodes running a single cache instance
  - Memcached clusters comprise of 1 to 20 nodes
  - Nodes are basically individual EC2 instances running the caching software
  - With redis, a cluster comprises of one single node, and there are up to six read replicas that can also be added to create a replica group
  - Redis is the industry leader for in-memory caching, so you should almost always choose Redis

### Configuring a Redis Cluster in ElastiCache

- We need to create a security group for the cluster
- The cluster will need to accept connections on port 6379, the default Redis port

### Interacting with ElastiCache in Code

- We won't be using the AWS SDK, instead, we'll just connect to it directly
- We need to add ElastiCache permissions to EC2 role

# AWS Developer: Designing and Developing

## 1. Designing and Developing on AWS

### Approaching Cloud Development on AWS

- In the past, developers didn't have to concern themselves with the operations side of application development
- Nowadays, operations concerns are consistently falling into developer's laps
- Many hats of developers:
  - Coding
  - Debugging
  - Testing
  - Operations
- Cloud development 101
  - Start with the infrastructure
- Cloud development decisions
  - Computing: EC2 or Lambda?
  - Persistence: S3, ElastiCache, RDS, or DynamoDB?
  - Routing: API Gateway or Route 53 DNS?

### Service Domains in AWS

- **Compute**
  - Sort of the most important domain
  - Runs code that you provide
  - Services:
    - EC2
    - Lambda
    - Elastic Beanstalk
    - Batch
- **Storage**
  - Stores data
  - Services:
    - Simple Storage Service (S3)
    - Elastic File System (EFS)
    - Glacier
    - Storage Gateway
- **Database**
  - Provides databases
  - Services:
    - Relational Database Service (RDS)
    - DynamoDB
    - ElastiCache
    - Amazon DocumentDB
- **Networking and Content Delivery**
  - Controls networking and routing
  - Services:
    - Virtual Private Cloud (VPC)
    - CloudFront
    - API Gateway
    - Route 53
- **Developer Tools**
  - Facilitates development lifecycle
  - Services:
    - CodeStar
    - CodeCommit
    - CodeBuild
    - CodeDeploy
    - CodePipeline
    - X-Ray
- **Management and Governance**
  - Service management and orchestration
  - Services:
    - CloudWatch
    - CloudFormation
    - CloudTrail
    - OpsWorks
- **Security, Identity and Compliance**
  - Manages service access
  - Services:
    - Identity and Access Management (IAM)
    - Certificate Manager
    - Secrets Manager
    - WAF and Shield
    - Key Management Service
- **Analytics**
  - Consumes and processes data
  - Services:
    - Redshift
    - Elastic Map Reduce (EMR)
    - OpenSearch
    - Kinesis
    - Data Pipeline
- **Application Integration**
  - Integrates AWS services
  - Services:
    - Simple Notification Service (SNS)
    - Simple Queue Service (SQS)
    - Simple Workflow (SWF)
    - Step Functions

### SDKs for AWS

- The course uses the [Javascript SDK](https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/index.html)

### Preparing for the AWS Developer Exam

- AWS Developer Associate Certification
  - Knowledge of AWS services
  - Options and details of each service
  - Different problems and scenarios when using each service
- Certification specifics
  - Multiple choice questions
  - Service specific topic, detail, or limit
  - Debugging and architecture
- Previously sighted on the exam
  - EC2
  - S3
  - DynamoDB
  - SQS
  - Route 53
  - Elastic Beanstalk

## 2. Launching Instances in AWS

### Overview

- EC2, AMI, AWS marketplace
- The dreaded EC2 REST API
- Creating an EC2 instance
- Look at different ways to manage EC2 instances
- Use an existing product from the AWS Marketplace to launch an instance that will have some preinstalled software
- With the instance created, create an AMI from that existing instance to get a good, reproducible image
- Look at the limits of the EC2 service and AMIs

### EC2, AMI, and the AWS Marketplace

- **EC2 structure**

![EC2 structure](./images/ec2-structure.PNG)

  - The basic unit in EC2 is the instance
  - It's a virtual, self-contained computing unit
  - Needs software installed on it to run (AMI)
- **AMI**
  - Operating systems
  - OS and software (vendor, OSS, etc.)
  - Create your own
  - Launch directly from AWS Marketplace
  - Create AMI from any EC2 instance
- **EC2 instance anatomy**

![EC2 instance anatomy](./images/ec2-instance-anatomy.PNG)

- **EC2 instance file system types**
  - Instance store (physically connected, basic hard drives)
  - Elastic Block Storage (independent, networked volume)
  - Elastic File System (scalable, independent, networked volume)
- **Launching AMI instances**
  | Instance Volume Backed AMI          | EBS Backed AMI                 |
  | ----------------------------------- | ------------------------------ |
  | Cannot be stopped                   | Can be stopped                 |
  | Can only be terminated or restarted | Instance data persisted on EBS |
  | Slower to boot                      | Faster to boot                 |
  | Data transferred from S3 on boot    | Data stored in EBS             |
- **AMI visibility**
  - **Public** - Visible to everyone
  - **Explicit** - Visible to who you allow
  - **Implicit** - Private

### EC2 Instance Classes

- Instances:
  - Spot instances
  - On-demand instances
  - Reserved instances
- In this course, only on-demand instances are used
- The instance class defines the conditions in which the instance lives and how you pay for it
  | On-demand Instances                   | Spot Instances                   | Reserved Instances                  |
  | ------------------------------------- | -------------------------------- | ----------------------------------- |
  | Pay for what you use                  | Bid for computing resources      | Commitment for a period of time     |
  | No commitment                         | Uses spare, unused EC2 capacity  | Commitments range from 1 to 3 years |
  | Easy to create and delete             | Discounts up to 90% off          | Discounts up to 75% off             |
  | Good for applications needing scaling | No guarantee                     | Good for stable companies           |
  | Good for learning and testing EC2     | Good for flexible computing jobs |                                     |

### The AWS REST API

- Each AWS REST API request must be signed
- The AWS SDK is a code interface to the REST API
- AWS REST API signature versions
  | Version 2                          | Version 4                                                   |
  | ---------------------------------- | ----------------------------------------------------------- |
  | Older version                      | Newer version                                               |
  | Supported by all older services    | Only version supported in newer regions (Ohio, Canada, etc) |
  | In the process of being deprecated | More secure, more complicated                               |

  ![Version 4 signing process](./images/v4-signing-process.PNG)

- Using the SDK means never having to manually sign a request
- AWS SDK uses local credentials to sign requests

### Creating an EC2 Security Group

- Components of an EC2 instance
  - Security group
  - Key pair

### Creating an EC2 Instance

- To create an instance, you must have a default VPC already configured
- The default subnets must auto-assign public IPV4 addresses to instances
- These are usually configured by default

### Limits with EC2 and AMI

- EC2 limits
  - Maximum number of running instances per region
- AMI limits
  - 10000 maximum AMIs (due to EBS snapshot restriction)

## 3. Scalable Computing in AWS

### Overview

- Scalability !== Elasticity
- Launching a launch template
- Balancing requests with an ELB (Load balancer)
- Auto scaling groups
- When to scale
- Limits to the scaling

### Understanding Scalability and Elasticity with AWS

- Scalability = increasing and decreasing the size or quantity of a resource in AWS
- Elasticity = scaling in response to preset rules, often triggered by CloudWatch alarms

### Creating a Launch Template

- Launch template is a blueprint for creating an EC2 instance
- Launch template attributes
  - AMI
  - Instance type
  - Security groups
  - User data
- Auto Scaling groups depend on launch templates
- Launch templates use versioning to differentiate changes in a single template

### Creating a Load Balancer

![Load balancer, target group](./images/load-balancer.PNG)

### Limits with Auto Scaling and ELB

- Auto scaling
  - Soft limit on number of groups and launch configurations
- Load balancing limit
  - Soft limit on number of application/network load balancers
  - One SSL certificate per load balancer
  - One load balancer per target group

## 4. Storage in AWS

### Considerations with EBS, EFS, and Instance Store

- EC2 instance store volumes
  - Physically connected hard drives
  - Previous default storage option
  - Data is gone if EC2 instance is stopped or terminated
  - Can't move data off instance store volumes easily
  - Can't create a data snapshot from instance store
- Elastic Block Store volumes
  - Can back up with snapshot
  - Can detach and reuse
  - EC2 instance can be stopped
  - Independent from EC2 instance
  - Are replicated within the Availability zones for maximum data durability
- EFS
  - Is built to connect to multiple instances at once and scale the file system as needed
  - Use to solve Big Data storage problems

### Reusing EBS Volumes with EC2

- EBS volumes with product codes can only be attached to stopped instances

### Versioning and Lifecycle Events in S3

- Versioning -> store previous versions of an object any time it's modified in any way
  - Affects cost
- Lifecycle events
  - Define rules for objects according to how long since an object's creation
  - Examples
    - Moving objects to cheaper storage
    - Expiration -> delete files a certain amount of time after they were created

### Limits with EBS and S3

- EBS
  - EC2 instance must be stopped before removing root EBS volume
  - EBS volumes with product codes can only be attached to stopped instances
  - EBS volumes can only attach to EC2 instances in the same Availability Zone
- S3
  - Soft limit of 100 buckets per account
  - S3 bucket names must be globally unique

## 5. Persistence in AWS

### How DynamoDB Throughput Capacity Works

- Throughput capacity is the number of records that can be read or written per second -> 4KB per unit for reading, 1KB per unit for writing
- Burst capacity
  - Used when throughput capacity is exceeded. No guarantees given from AWS of burst capacity availability
- DynamoDB read types
  - Eventual Consistency -> may not have recent changes
  - Strong consistency -> Guarantees newest changes
- DynamoDB capacity modes
  - Provisioned capacity
    - Configure # of read/write requests
    - Overage requests may be rejected
    - Auto scaling adjusts requests based on traffic
    - Use it or lose it
  - On-demand mode
    - Only charged for each read/write request
    - Pay for what you use
    - More expensive per request than Provisioned
    - Scales as needed with no configuration

### DynamoDB Keys and Secondary Indexes

- Partition key (hash attribute)
  - Used by DynamoDB table to determine which partition to put a record. Must be unique if no range key used
  
![Partition key usage](./images/partition-key.PNG)

- Sort key (range attribute)
  - Used in conjunction with a partition key to sort documents with the same partition key in a partition
- DynamoDB secondary index types
  - Global secondary index
    - Define new key schema
    - Define record attributes to include in index
    - Independent provisioned throughput
  - Local secondary index
    - Define additional sort key only
    - Original partition key + new sort key used
    - All base table attributes available

### Querying a DynamoDB Table

- Table scan
  - Retrieves all records from a table, 1MB at a time

## 6. Routing from AWS

### Route 53 Routing Policies

- Simple
  - Single DNS records from one source to one AWS resource
  - For instance, sending the domain name hbfl.online to a single load balancer
- Weighted
  - Assigns weights to multiple resources and divide traffic between them according to those weights
- Latency
  - Route traffic to resources with the lowest latency for a region
- Geolocation
  - Route traffic to specific resources based on the location of the requester
- Failover
  - Configure a backup resource to route traffic to if the primary resource fails

### Creating a Hosted Zone in Route 53

- Route 53 hosted zone
  - Resource that maps to a domain name through which you can configure subdomains and other routing rules
- Route 53 record set
  - DNS rules that send traffic from a source to an AWS resource

### What is API Gateway?

- Essentially a routing layer that allows to configure many rules on the same domain and connect it to many different resources

![API gateway](./images/api-gateway.PNG)
![API structure in API gateway](./images/api-structure-api-gateway.PNG)
![API gateway method](./images/api-gateway-method.PNG)

### Deploying an API in API Gateway

- API gateway stages

![API gateway stages](./images/api-gateway-stages.PNG)

### Limits with Route 53 and API Gateway

- Route 53
  - Indeterminable delay between DNS changes
  - Soft limits on hosted zones, domains, and record sets
- API Gatewway
  - Change request hard limit that is different for each change type

## 7. Delivering Content with AWS

### Understanding Cloudfront Invalidations

- Fastest when served from CloudFront Cache, not from your origin
- Max TTL
  - The maximum amount of time an object will stay in CloudFront cache before a new copy is retrieved from the origin. The higher, the better. Default is one year
- CloudFront invalidations
  - Replaces a designated object with a fresh copy from the origin
  - Can contain multiple files or directories
  - 3000 object invalidation hard limit
  - 1000 free invalidations/month
  - Can take minutes to process
- Immutable objects in CloudFront
  - Can be cached for maximum TTL and no invalidations needed
  - Each object has unique name (eg. add hash)
  - CloudFront retrieves from origin once

### The Science Behind S3 Static Website Hosting

  | Website                       | S3 Bucket                |
  | ----------------------------- | ------------------------ |
  | Request Path and receive HTML | Use key to get an object |
  | No direct referencing needed  | Direct referencing       |

- Configure an S3 bucket to behave like a website

## 8. Messaging Inside AWS

### Understanding SQS Polling and the Message Lifecycle

- Delay seconds
  - Amount of time to delay the visibility of an incoming message
- Visibility timeout
  - Amount of time to make the message invisible after it has been read by a consumer

### Understanding Kinesis Streams

- Real-time data streaming
- Scales quickly and easily
- Multiple producers and consumers
- Kinesis stream shards
  - Basic unit of Kinesis Stream capacity. One shard equals 1MB/second of input and 2MB/second of output

## 9. Communicating with AWS